#include "game.h"

Game::Game() : BaseGame("~*~FLOAT~*~ Game2", sf::Vector2u(GAME_WINDOW_WIDTH, GAME_WINDOW_HEIGHT))
{

}

Game::~Game()
{
    m_window.~Window();
}

void Game::handleInput()
{

}

void Game::update()
{
    m_window.update();
}

void Game::render()
{
    m_window.beginDraw();

    // Custom render

    m_window.endDraw();
}
