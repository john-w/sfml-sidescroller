#ifndef EVENTMANAGER_H
#define EVENTMANAGER_H

#include <vector>
#include <unordered_map>
#include <functional>

#include <SFML/Graphics.hpp>

#define FILE_KEYBINDINGS "keys.cfg"

namespace ev {
    enum class EventType;
    struct EventInfo;
    struct EventDetails;
    struct Binding;

    using Events = std::vector<std::pair<EventType, EventInfo>>;
    using Bindings = std::unordered_map<std::string, Binding*>;
    using Callbacks = std::unordered_map<std::string, std::function<void(EventDetails*)>>;

    enum class EventType {
        KeyDown = sf::Event::KeyPressed,
        KeyUp = sf::Event::KeyReleased,
        MButtonDown = sf::Event::MouseButtonPressed,
        MButtonUp = sf::Event::MouseButtonReleased,
        MouseWheel = sf::Event::MouseWheelMoved,
        WindowResized = sf::Event::Resized,
        GainedFocus = sf::Event::GainedFocus,
        LostFocus = sf::Event::LostFocus,
        MouseEntered = sf::Event::MouseEntered,
        MouseLeft = sf::Event::MouseLeft,
        Closed = sf::Event::Closed,
        TextEntered = sf::Event::TextEntered,

        // Extensions
        Keyboard = sf::Event::Count + 1,
        Mouse,
        Joystick
    };

    struct EventInfo {
        union {
            int m_code;
        };
        EventInfo() {m_code = 0;}
        EventInfo(int l_event) {m_code = l_event;}
    };

    struct EventDetails {
        std::string m_name;
        sf::Vector2i m_size;
        sf::Uint32 m_textEntered;
        sf::Vector2i m_mouse;
        int m_mouseWheelDelta;
        int m_keyCode;

        EventDetails(const std::string &l_bindName) : m_name(l_bindName) {
            clear();
        }

        void clear() {
            m_size = sf::Vector2i(0,0);
            m_textEntered = 0;
            m_mouse = sf::Vector2i(0,0);
            m_mouseWheelDelta = 0;
            m_keyCode = -1;
        }
    };

    struct Binding {
        Events m_events;
        std::string m_name;
        int count;

        EventDetails m_details;

        Binding(const std::string &l_name)
            : m_name(l_name), m_details(l_name), count(0) {};

        void bindEvent(EventType l_type, EventInfo l_info = EventInfo()) {
            m_events.emplace_back(l_type, l_info);
        }
    };

    class EventManager {
        private:
        Bindings m_bindings;
        Callbacks m_callbacks;
        bool m_hasFocus;

        public:
        EventManager();
        ~EventManager();

        bool addBinding(Binding *l_binding);
        bool removeBinding(std::string l_name);
        void setFocus(const bool &l_focus);

        template<class T>
        bool addCallback(const std::string &l_name, void(T::*l_func)(EventDetails*),
                         T* l_instance) {
            auto temp = std::bind(l_func, l_instance, std::placeholders::_1);
            return m_callbacks.emplace(l_name, temp).second;
        }

        void removeCallback(const std::string &l_name) {
            m_callbacks.erase(l_name);
        }

        void handleEvent(sf::Event &l_event);
        void update();

        sf::Vector2i getMousePos(sf::RenderWindow* l_window = nullptr) {
            return (l_window ? sf::Mouse::getPosition(*l_window) : sf::Mouse::getPosition());
        }

        private:
        void loadBindings();
    };
}
#endif // EVENTMANAGER_H
