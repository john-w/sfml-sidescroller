#ifndef BASE_GAME_H
#define BASE_GAME_H

#include <SFML/Graphics.hpp>
#include "window.h"

class BaseGame
{
    private:
        sf::Clock m_clock;
        sf::Time m_elapsed;

    protected:
        Window m_window;

    public:
        BaseGame() : m_window("App Window", sf::Vector2u(800,600)) {}
        BaseGame(const std::string& l_title, const sf::Vector2u& l_size) : m_window(l_title, l_size) {};
        ~BaseGame() {}

        virtual void handleInput() = 0;
        virtual void update() = 0;
        virtual void render() = 0;

        void restartClock() { m_elapsed = m_clock.restart(); }
        sf::Time getElapsedTime() { return m_elapsed; }

        Window *getWindow() { return &m_window; }
};


#endif // BASE_GAME_H
