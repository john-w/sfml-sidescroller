#include "window.h"

Window::Window()
{
    setup("Window", sf::Vector2u(800,600));
}

Window::Window(const std::string &l_title, const sf::Vector2u &l_size)
{
    setup(l_title, l_size);
}

Window::~Window()
{
    destroy();
}

void Window::beginDraw()
{
    m_window.clear(m_clearColor);
}

void Window::endDraw()
{
    m_window.display();
}

void Window::update()
{
    sf::Event event;
    while (m_window.pollEvent(event))
    {
        if (event.type == sf::Event::LostFocus)
        {
            m_isFocused = false;
            m_eventManager.setFocus(false);
        }
        else if (event.type == sf::Event::GainedFocus)
        {
            m_isFocused = true;
            m_eventManager.setFocus(true);
        }
        m_eventManager.handleEvent(event);
    }
    m_eventManager.update();
}

bool Window::isDone()
{
    return m_isDone;
}

bool Window::isFullscreen()
{
    return m_isFullScreen;
}

bool Window::isFocused()
{
    return m_isFocused;
}

sf::Vector2u Window::getWindowSize()
{
    return m_windowSize;
}

sf::RenderWindow* Window::getRenderWindow()
{
    return &m_window;
}

ev::EventManager *Window::getEventManager()
{
    return &m_eventManager;
}

void Window::toggleFullscreen(ev::EventDetails *l_details)
{
    m_isFullScreen = !m_isFullScreen;
    destroy();
    create();
}

void Window::close(ev::EventDetails *l_details)
{
    m_isDone = true;
}

void Window::draw(sf::Drawable &l_drawable)
{
    m_window.draw(l_drawable);
}

void Window::setup(const std::string &l_title, const sf::Vector2u &l_size)
{
    m_windowTitle = l_title;
    m_windowSize = l_size;
    m_isFullScreen = false;
    m_isDone = false;
    m_isFocused = true;

    // Bind window events
    m_eventManager.addCallback("fullscreen-toggle", &Window::toggleFullscreen, this);
    m_eventManager.addCallback("window-close", &Window::close, this);

    create();
}

void Window::destroy()
{
    m_window.close();
}

void Window::create()
{
    auto style = (m_isFullScreen ? sf::Style::Fullscreen : sf::Style::Default);
    m_window.create({m_windowSize.x, m_windowSize.y, 32}, m_windowTitle, style);
    m_window.setFramerateLimit(60);
}
