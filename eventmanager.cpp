#include "eventmanager.h"
#include <fstream>
#include <iostream>
#include <sstream>

ev::EventManager::EventManager() : m_hasFocus(true)
{
    loadBindings();
}

ev::EventManager::~EventManager()
{
    for (auto &bind : m_bindings) {
        delete bind.second;
        bind.second = nullptr;
    }
}

bool ev::EventManager::addBinding(ev::Binding *l_binding)
{
    if (m_bindings.find(l_binding->m_name) != m_bindings.end())
        return false;

    return m_bindings.emplace(l_binding->m_name, l_binding).second;
}

bool ev::EventManager::removeBinding(std::string l_name)
{
    auto bind = m_bindings.find(l_name);
    if (bind == m_bindings.end()) {return false;}

    delete bind->second;
    m_bindings.erase(bind);
    return true;
}

void ev::EventManager::setFocus(const bool &l_focus)
{
    m_hasFocus = l_focus;
}

void ev::EventManager::handleEvent(sf::Event &l_event)
{
    for (auto &bind_itr : m_bindings)
    {
        Binding *bind = bind_itr.second;
        for (auto &ev_itr : bind->m_events)
        {
            EventType sfmlev = (EventType) l_event.type;
            if (ev_itr.first != sfmlev) {continue;}
            if (sfmlev == EventType::KeyDown || sfmlev == EventType::KeyUp)
            {
                if (ev_itr.second.m_code == l_event.key.code)
                {
                    if (bind->m_details.m_keyCode != -1)
                        bind->m_details.m_keyCode = ev_itr.second.m_code;

                    ++(bind->count);
                    break;
                }
            }
            else if (sfmlev == EventType::MButtonDown || sfmlev == EventType::MButtonUp)
            {
                if (ev_itr.second.m_code == l_event.mouseButton.button)
                {
                    bind->m_details.m_mouse.x = l_event.mouseButton.x;
                    bind->m_details.m_mouse.y = l_event.mouseButton.y;
                    if (bind->m_details.m_keyCode != -1)
                        bind->m_details.m_keyCode = ev_itr.second.m_code;
                    ++(bind->count);
                    break;
                }
            }
            else
            {
                if (sfmlev == EventType::MouseWheel)
                    bind->m_details.m_mouseWheelDelta = l_event.mouseWheel.delta;
                else if (sfmlev == EventType::WindowResized) {
                    bind->m_details.m_size.x = l_event.size.width;
                    bind->m_details.m_size.y = l_event.size.height;
                }
                else if (sfmlev == EventType::TextEntered)
                    bind->m_details.m_textEntered = l_event.text.unicode;
                ++(bind->count);
            }
        }
    }
}

void ev::EventManager::update()
{
    if (!m_hasFocus) return;

    for (auto &bind_itr : m_bindings)
    {
        Binding *bind = bind_itr.second;
        for (auto &ev_itr : bind->m_events)
        {
            switch (ev_itr.first)
            {
                case EventType::Keyboard:
                    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key(ev_itr.second.m_code)))
                    {
                        if (bind->m_details.m_keyCode != -1)
                            bind->m_details.m_keyCode = ev_itr.second.m_code;
                        ++(bind->count);
                    }
                    break;
                case EventType::Mouse:
                    if (sf::Mouse::isButtonPressed(sf::Mouse::Button(ev_itr.second.m_code)))
                    {
                        if (bind->m_details.m_keyCode != -1)
                            bind->m_details.m_keyCode = ev_itr.second.m_code;
                        ++(bind->count);
                    }
                    break;
                case EventType::Joystick:
                    break;
            }
            if (bind->m_events.size() == bind->count)
            {
                auto callItr = m_callbacks.find(bind->m_name);
                if (callItr != m_callbacks.end())
                    callItr->second(&bind->m_details);

            }

        }
        bind->count = 0;
        bind->m_details.clear();
    }
}

void ev::EventManager::loadBindings()
{
    std::string delimiter = ":";

    std::ifstream bindings;
    bindings.open(FILE_KEYBINDINGS);
    if (!bindings.is_open()){
        std::cout << "[!] Failed loading " << FILE_KEYBINDINGS << " bindings file." << std::endl;
        return;
    }

    std::string line;
    while (std::getline(bindings, line))
    {
        std::stringstream keystream(line);
        std::string callbackName;
        keystream >> callbackName;
        Binding* bind = new Binding(callbackName);
        while (!keystream.eof())
        {
            std::string keyval;
            keystream >> keyval;
            int start = 0;
            int end = keyval.find(delimiter);
            if (end == std::string::npos)
            {
                delete bind;
                bind = nullptr;
                break;
            }
            EventType type = EventType(stoi(keyval.substr(start, end - start)));
            int code = stoi(keyval.substr(
                                end + delimiter.length(),
                                keyval.find(delimiter, end+delimiter.length())
                                ));
            EventInfo eventInfo(code);

            bind->bindEvent(type, eventInfo);
        }
        if (!addBinding(bind)) {delete bind;}
        bind = nullptr;
    }
    bindings.close();
}
