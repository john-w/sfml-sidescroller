#ifndef GAME_H
#define GAME_H

#include "base_game.h"

#define GAME_WINDOW_WIDTH 800
#define GAME_WINDOW_HEIGHT 600

class Game : public BaseGame
{
    public:
    Game();
    ~Game();

    void handleInput();
    void update();
    void render();
};

#endif // GAME_H
