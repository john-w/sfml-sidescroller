#ifndef TEXTBOX_H
#define TEXTBOX_H

#include <SFML/Graphics.hpp>
#include <vector>
#include <string>

#define C_BACKDROP sf::Color(90,90,90,90)
#define C_TEXT sf::Color::White

using MessageContainer = std::vector<std::string>;

class Textbox
{
    private:
    MessageContainer m_messages;
    int m_numVisible;

    sf::RectangleShape m_backdrop;
    sf::Font m_font;
    sf::Text m_content;

    public:
    Textbox();
    Textbox(int l_visible, int l_charSize, int l_width, sf::Vector2f l_screenPos);
    ~Textbox();

    void setup(int l_visible, int l_charSize, int l_width, sf::Vector2f l_screenPos);
    void add(std::string l_message);
    void clear();

    void render(sf::RenderWindow &l_window);
};

#endif // TEXTBOX_H
