#ifndef WINDOW_H
#define WINDOW_H

#include <SFML/Graphics.hpp>
#include "eventmanager.h"

class Window {
    private:
    ev::EventManager m_eventManager;
    sf::RenderWindow m_window;
    sf::Vector2u m_windowSize;
    std::string m_windowTitle;
    bool m_isDone;
    bool m_isFullScreen;
    bool m_isFocused;
    sf::Color m_clearColor = sf::Color(12,12,90);   // Dark blue

    public:
    Window();
    Window(const std::string& l_title, const sf::Vector2u& l_size);
    ~Window();

    void beginDraw();
    void endDraw();
    void update();

    bool isDone();
    bool isFullscreen();
    bool isFocused();
    sf::Vector2u getWindowSize();
    sf::RenderWindow *getRenderWindow();
    ev::EventManager *getEventManager();

    void toggleFullscreen(ev::EventDetails *l_details);
    void close(ev::EventDetails *l_details = nullptr);

    void draw(sf::Drawable& l_drawable);

    private:
    void setup(const std::string& l_title, const sf::Vector2u& l_size);
    void destroy();
    void create();
};
#endif // WINDOW_H
